(ns robot.test.script
  (:import [java.io IOException])
  (:use [robot.script] :reload)
  (:use [robot.core]
	[clojure.test]))

(deftest header-parser
  (testing "Should"
    (testing "parse grid header line"
      (are [line expected] (= expected (parse-grid-line line))
	   "8 3" [8 3]
	   "10 12" [10 12]))
    
    (testing "not parse invalid grid header line"
      (are [line] (thrown? IOException (parse-grid-line line))
	   "10 3A"
	   "A3 1"))
    
    (testing "parse location header line"
      (are [line expected] (= expected (parse-location-line line))
	   "8 3 N" '[(8 3) "N"]
	   "10 12 S" '[(10 12) "S"]))
    
    (testing "not parse invalid location header line"
      (are [line] (thrown? IOException (parse-location-line line))
	   "8 3"
	   "8 3 Z"
	   "10 3A S"))))

(deftest command-parser
  (testing "Should"
    (testing "process sequences of movement commands"
      (let [robot (new-robot)
	    _ (process-commands! "LLRMRMR" robot)]
	(are [key expected] (= expected (key @robot))
	     :direction "E"
	     :location [0 1])))
    
    (testing "ignore empty sequences of commands"
      (let [robot (new-robot)
	    _ (process-commands! "" robot)]
	(are [key expected] (= expected (key @robot))
	     :direction "N"
	     :location [0 0])))
    
    (testing "ignore an invalid sequence of movement commands"
      (let [robot (new-robot)
	    _ (process-commands! "LLRXRMR" robot)]
	(are [key expected] (= expected (key @robot))
	     :direction "N"
	     :location [0 0])))
    
    (testing "process a teleport command"
      (let [robot (new-robot)
	    _ (process-commands! "T 6 2" robot)]
	(are [key expected] (= expected (key @robot))
	     :direction "N"
	     :location [6 2])))
    
    (testing "ignore an invalid teleport command"
      (are [line] (let [robot (new-robot) _ (process-commands! line robot)]
		    (is (= [0 0] (:location @robot))))
	   "T 62"
	   "T 6A 2"
	   "Z 6 2"))))

(deftest script-parser
  (testing "Should"
    (testing "process script"
      (is (= "4 6 E" (process "10 10\n2 5 N\nT 1 3\nLLRRMMMRLRMMM"))))
    
    (testing "not process script without header"
      (are [line] (thrown? IOException (process line))
	   "10 10\nT 1 3\nLLRRMMMRLRMMM"
	   "2 5 N\n10 10\nT 1 3\nLLRRMMMRLRMMM"
	   "2 5 N\nT 1 3\nLLRRMMMRLRMMM"
	   "T 1 3\nLLRRMMMRLRMMM"))
    
    (testing "process empty script with valid header"
      (is (= "3 5 N" (process "10 10\n3 5 N\n"))))))