(ns robot.main
  (:use [clojure.contrib.command-line]
	[robot.script]))

(with-command-line *command-line-args*
  "Robot script file parser"
  [[file f "Input script file"]]
  (println (process (slurp file))))