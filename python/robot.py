#!/usr/bin/env python
#-*- coding:utf-8 -*-

import re

class Robot(object):
    """Solution of the robot problem.
    """

    # Direction vectors and transitions for all possible states
    __states = {'N': ((0, 1), ('W','E')),
                'S': ((0,-1), ('E','W')),
                'E': ((1, 0), ('N','S')),
                'W': ((-1,0), ('S','N'))}

    def __init__(self, grid=(10,10), location=(0,0), direction='N'):
        self.grid = grid
        self.location = location
        self.direction = direction

    def _set_direction(self, direction):
        """Updates the robot state based on its current direction.
        """
        self.direction = direction
        return self.direction
 
    def turn_left(self):
        """Turns this robot left while keeping its current location.
        """
        directions = self.__states[self.direction][1]
        return self._set_direction(directions[0])

    def turn_right(self):
        """Turns this robot right while keeping its current location.
        """
        directions = self.__states[self.direction][1]
        return self._set_direction(directions[1])

    def _is_location_valid(self, pos):
        """Returns whether the x,y coordinate tuple `pos` is valid.
        """
        x, y = pos
        return x >= 0 and x < self.grid[0] and \
               y >= 0 and y < self.grid[1]

    def move(self):
        """Moves this robot one unit to the direction direction it's currently
        facing.
        """
        loc, pos = self.location, self.__states[self.direction][0]
        next = (loc[0] + pos[0]), (loc[1] + pos[1])
        return self.teleport(next)

    def teleport(self, to):
        """Moves this robot to the given x,y coordinate tuple `to`.
        """
        if self._is_location_valid(to):
            self.location = to
        return self.location


class RobotScript(object):
    """Parser that reads commands from a script text and sends them to a Robot.
    """

    # Regular expressions to parse the input data
    __re_grid     = re.compile(r'^(?P<x>\d+) (?P<y>\d+)$')
    __re_location = re.compile(r'^(?P<x>\d+) (?P<y>\d+) (?P<direction>[NSEW])$')
    __re_commands = re.compile(r'^(?P<commands>[LRM]+)$')
    __re_teleport = re.compile(r'^T (?P<x>\d+) (?P<y>\d+)$')

    def __init__(self, script):
        self.script = script or []
        self.robot = Robot()

    def process(self):
        """Processes each input line from the script file and returns the
        robot's final location as a string, e.g. 'x y C', where `x y` is the
        coordinate and `C` is the facing direction.
        """
        self._read_header()

        for line in self.script[2:]:
            self._process_commands(line)

        loc = self.robot.location
        direction = self.robot.direction
        return '%d %d %s' % (loc[0], loc[1], direction)

    def _read_header(self):
        """Reads the input file header and sets the robot's initial state.
        """
        if len(self.script) < 2:
            raise SyntaxError('Header not found')
        grid = self._parse_grid_line(self.script[0])
        location, direction = self._parse_location_line(self.script[1])
        self.robot = Robot(grid, location, direction)

    def _parse_grid_line(self, line):
        """Parses the grid data header.
        """
        grid_data = self.__re_grid.match(line)
        if not grid_data:
            raise SyntaxError('Grid dimension expected, ex: "10 10"')
        re_dict = grid_data.groupdict()
        x, y = int(re_dict['x']), int(re_dict['y'])
        return (x, y)

    def _parse_location_line(self, line):
        """Parses the location data header.
        """
        location_data = self.__re_location.match(line)
        if not location_data:
            raise SyntaxError('Location data expected, ex: "5 2 N"')
        re_dict = location_data.groupdict()
        x, y = int(re_dict['x']), int(re_dict['y'])
        return ((x,y), re_dict['direction'])

    def _process_commands(self, line):
        """Processes an input line.
        """
        if not line or line.isspace():
            return
        for line_re, func in self.__line_parsers:
            line_data = line_re.match(line)
            if line_data:
                return func(self, line_data.groupdict())
        else:
            raise SyntaxError('Invalid command line')

    def _parse_commands(self, re_dict):
        """Processes movement commands.
        """
        for command in re_dict['commands']:
            if command == 'L': self.robot.turn_left()
            elif command == 'R': self.robot.turn_right()
            elif command == 'M': self.robot.move()

    def _parse_teleport(self, re_dict):
        """Processes a teleport command.
        """
        x, y = int(re_dict['x']), int(re_dict['y'])
        self.robot.teleport((x,y))

    # Parsers for each type of input line
    __line_parsers = (
        (__re_commands, _parse_commands),
        (__re_teleport, _parse_teleport),
    )


def main():
    """Parses the given input file and prints the robot's final location.
    """
    import optparse
    parser = optparse.OptionParser()
    parser.add_option('-f', '--file', dest='filename',
        help='Read commands from FILE')

    options = parser.parse_args()[0]
    if not options.filename:
        parser.print_help()
        return

    script = RobotScript(open(options.filename, 'r').readlines())
    print(script.process())


if __name__ == '__main__':
    main()
