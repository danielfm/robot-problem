(ns robot.test.core
  (:use [robot.core] :reload)
  (:use [clojure.test]))

(deftest robot-initialization
  (testing "Robot should be initialized"
    (testing "with sane defaults"
      (let [robot (new-robot)]
	(are [key val] (= (key @robot) val)
	     :grid [10 10]
	     :direction "N"
	     :location [0 0])))
    
    (testing "with a custom direction"
      (let [robot (new-robot :direction "W")]
	(are [key val] (= (key @robot val))
	     :grid [10 10]
	     :direction "W"
	     :location [0 0])))

    (testing "with a custom grid size"
      (let [robot (new-robot :grid [20 20])]
	(are [key val] (= (key @robot val))
	     :grid [20 20]
	     :direction "N"
	     :location [0 0])))

    (testing "with a custom location"
      (let [robot (new-robot :location [5 2])]
	(are [key val] (= (key @robot val))
	     :grid [10 10]
	     :direction "N"
	     :location [5 2])))))

(deftest robot-movements
  (testing "Robot should"
    (testing "turn left"
      (let [robot (new-robot)]
	(are [dir] (= dir (:direction (turn-left! robot)))
	     "W" "S" "E" "N" "W")))

    (testing "turn right"
      (let [robot (new-robot)]
	(are [dir] (= dir (:direction (turn-right! robot)))
	     "E" "S" "W" "N" "E")))

    (testing "move forward"
      (let [robot (new-robot)]
	(are [pos func] (= pos (:location (func robot)))
	     [0 1] move!
	     [0 2] move!
	     [0 2] turn-right!
	     [1 2] move!
	     [2 2] move!)))

    (testing "not leave the grid"
      (testing "lower limits"
	(let [robot (new-robot)]
	  (are [pos func] (= pos (:location (func robot)))
	       [0 0] turn-left!
	       [0 0] move!
	       [0 0] turn-left!
	       [0 0] move!)))
      
      (testing "upper limits"
	(let [robot (new-robot :location [9 9])]
	  (are [pos func] (= pos (:location (func robot)))
	       [9 9] turn-right!
	       [9 9] move!
	       [9 9] turn-left!
	       [9 9] move!))))

    (testing "teleport itself to another position"
      (let [robot (new-robot)]
	(are [cur new] (= cur (:location (teleport! robot new)))
	     [2 3] [2 3] ;; [expected_x expected_y] [new_x new_y]
	     [3 4] [3 4]
	     [0 0] [0 0]
	     [9 9] [9 9])))

    (testing "not teleport itself outside the grid"
      (let [robot (new-robot)]
	(are [cur new] (= cur (:location (teleport! robot new)))
	     [0 0] [-1 0]
	     [0 0] [0 -1]
	     [0 0] [-1 -1]
	     [0 0] [10 9]
	     [0 0] [9 10]
	     [0 0] [10 10])))))