#!/usr/bin/env python
#-*- coding:utf-8 -*-

import unittest

from robot import *

class RobotTestCase(unittest.TestCase):
    """Test cases for the Robot class.
    """
    def setUp(self):
        self.robot = Robot()

    def test_create_default_robot(self):
        """Robot should be initialized with sane defaults.
        """
        self.assertEqual((10,10), self.robot.grid)
        self.assertEqual((0,0), self.robot.location)
        self.assertEqual('N', self.robot.direction)

    def test_create_robot_facing_west(self):
        """Robot should be initialized with a custom direction.
        """
        self.robot = Robot(direction='W')
        self.assertEqual((10,10), self.robot.grid)
        self.assertEqual((0,0), self.robot.location)
        self.assertEqual('W', self.robot.direction)

    def test_create_robot_for_a_20_20_grid(self):
        """Robot should be initialized with a custom grid size.
        """
        self.robot = Robot(grid=(20,20))
        self.assertEqual((20,20), self.robot.grid)
        self.assertEqual((0,0), self.robot.location)
        self.assertEqual('N', self.robot.direction)

    def test_create_robot_in_custom_location(self):
        """Robot should be initialized with a custom location.
        """
        self.robot = Robot(location=(5,2))
        self.assertEqual((10,10), self.robot.grid)
        self.assertEqual((5,2), self.robot.location)
        self.assertEqual('N', self.robot.direction)

    def test_turn_left(self):
        """Robot should turn left.
        """
        self.assertEqual('W', self.robot.turn_left())
        self.assertEqual('W', self.robot.direction)
        self.assertEqual('S', self.robot.turn_left())
        self.assertEqual('E', self.robot.turn_left())
        self.assertEqual('N', self.robot.turn_left())
        self.assertEqual('W', self.robot.turn_left())

    def test_turn_right(self):
        """Robot should turn right.
        """
        self.assertEqual('E', self.robot.turn_right())
        self.assertEqual('E', self.robot.direction)
        self.assertEqual('S', self.robot.turn_right())
        self.assertEqual('W', self.robot.turn_right())
        self.assertEqual('N', self.robot.turn_right())
        self.assertEqual('E', self.robot.turn_right())

    def test_teleport(self):
        """Robot should teleport itself to another location.
        """
        self.assertEqual((2,3), self.robot.teleport((2,3)))
        self.assertEqual((2,3), self.robot.location)

        grid = self.robot.grid
        limit = (grid[0]-1, grid[1]-1)
        self.assertEqual(limit, self.robot.teleport(limit))

    def test_teleport_invalid(self):
        """Robot should not teleport itself outside the grid.
        """
        self.assertEqual((0,0), self.robot.teleport((-1,0)))
        self.assertEqual((0,0), self.robot.teleport((0,-1)))
        self.assertEqual((0,0), self.robot.teleport((0,10)))
        self.assertEqual((0,0), self.robot.teleport((10,0)))
        self.assertEqual((0,0), self.robot.teleport((10,-1)))

    def test_move(self):
        """Robot should move forward.
        """
        self.assertEqual((0,1), self.robot.move())
        self.assertEqual((0,1), self.robot.location)
        self.assertEqual((0,2), self.robot.move())

        self.robot.turn_right()
        self.assertEqual((1,2), self.robot.move())
        self.assertEqual((2,2), self.robot.move())

    def test_move_invalid(self):
        """Robot should not leave the grid.
        """
        self.robot.turn_left()
        self.assertEqual((0,0), self.robot.move())
        self.robot.turn_left()
        self.assertEqual((0,0), self.robot.move())

        # Upper boundaries
        self.robot.turn_left()
        self.robot.teleport((9,9))
        self.assertEqual((9,9), self.robot.move())
        self.robot.turn_left()
        self.assertEqual((9,9), self.robot.move())


class RobotScriptTestCase(unittest.TestCase):
    """Test cases for the RobotScript class.
    """
    def setUp(self):
        self.script = RobotScript(None)

    def test_parse_grid_line(self):
        """RobotScript should parse the grid configuration line.
        """
        self.assertEqual((8,3), self.script._parse_grid_line('8 3'))
        self.assertEqual((10,12), self.script._parse_grid_line('10 12'))

    def test_parse_grid_line_invalid(self):
        """RobotScript should not parse an invalid grid configuration line.
        """
        self.assertRaises(SyntaxError, self.script._parse_grid_line, '10 3A')
        self.assertRaises(SyntaxError, self.script._parse_grid_line, 'A3 1')

    def test_parse_location_line(self):
        """RobotScript should parse the location configuration line.
        """
        self.assertEqual(((8,3), 'N'), self.script._parse_location_line('8 3 N'))
        self.assertEqual(((10,12), 'S'), self.script._parse_location_line('10 12 S'))

    def test_parse_location_line_invalid(self):
        """RobotScript should not parse an invalid location configuration line.
        """
        self.assertRaises(SyntaxError, self.script._parse_location_line, '8 3')
        self.assertRaises(SyntaxError, self.script._parse_location_line, '8 3 Z')
        self.assertRaises(SyntaxError, self.script._parse_location_line, '10 3A S')

    def test_process_commands(self):
        """RobotScript should process sequences of movement commands.
        """
        self.script._process_commands('LLRMRMR')
        robot = self.script.robot
        self.assertEqual('E', robot.direction)
        self.assertEqual((0,1), robot.location)

    def test_process_commands_empty(self):
        """RobotScript should ignore empty sequences of commands.
        """
        self.script._process_commands('')
        robot = self.script.robot
        self.assertEqual('N', robot.direction)
        self.assertEqual((0,0), robot.location)

    def test_process_commands_invalid(self):
        """RobotScript should not process an invalid sequence of movement commands.
        """
        self.assertRaises(SyntaxError, self.script._process_commands, 'LLRXRMR')

    def test_process_teleport(self):
        """RobotScript should process a teleport command.
        """
        self.script._process_commands('T 6 2')
        robot = self.script.robot
        self.assertEqual('N', robot.direction)
        self.assertEqual((6,2), robot.location)

    def test_process_teleport_invalid(self):
        """RobotScript should not process an invalid teleport command.
        """
        self.assertRaises(SyntaxError, self.script._process_commands, 'T 62')
        self.assertRaises(SyntaxError, self.script._process_commands, 'T 6A 2')
        self.assertRaises(SyntaxError, self.script._process_commands, 'Z 6 2')

    def test_process(self):
        """RobotScript should process script.
        """
        self.script = RobotScript(['10 10\n', '2 5 N\n', 'T 1 3\n', 'LLRRMMMRLRMMM\n'])
        self.assertEqual('4 6 E', self.script.process());

    def test_process_without_header(self):
        """RobotScript should not process script without header.
        """
        self.assertRaises(SyntaxError, self.script.process);

    def test_process_empty_with_valid_header(self):
        """RobotScript should process empty script with valid header.
        """
        self.script = RobotScript(['10 10\n', '3 5 N\n'])
        self.assertEqual('3 5 N', self.script.process());


if __name__ == '__main__':
    unittest.main()
