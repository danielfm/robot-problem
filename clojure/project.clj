(defproject robot "1.0.0-SNAPSHOT"
  :description "Solution to the robot problem."
  :dependencies [[org.clojure/clojure "1.2.0"]
		 [org.clojure/clojure-contrib "1.2.0"]]
  :dev-dependencies [[swank-clojure "1.2.1"]
		     [lein-run "1.0.0-SNAPSHOT"]]
  :run-aliases {:script ["src/robot/main.clj"]})
