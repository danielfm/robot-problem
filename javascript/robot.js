var Robot = function(params) {
  var self = this;
  params = params || {};

  // Direction vectors and transitions for all possible states
  var states = {
    N: [[0, 1], ['W','E']],
    S: [[0,-1], ['E','W']],
    E: [[1, 0], ['N','S']],
    W: [[-1,0], ['S','N']]
  };

  this.grid = [10,10];
  this.position = [0,0];
  this.direction = 'N';

  // Default values
  if (params.grid && params.grid.length > 0) {
    this.grid = params.grid;
  }
  if (params.position && params.position.length > 0) {
    this.position = params.position;
  }
  if (params.direction) {
    this.direction = params.direction;
  }

  function isPositionValid(loc) {
    var x = loc[0], y = loc[1];
    return x >= 0 && x < self.grid[0] &&
           y >= 0 && y < self.grid[1];
  }

  // Member functions
  this.turnLeft = function() {
    var directions = states[this.direction][1];
    return (this.direction = directions[0]);
  };

  this.turnRight = function() {
    var directions = states[this.direction][1];
    return (this.direction = directions[1]);
  };

  this.move = function() {
    var loc = this.position, pos = states[this.direction][0];
    var x = (loc[0] + pos[0]), y = (loc[1] + pos[1]);
    return this.teleport([x,y]);
  };

  this.teleport = function(to) {
    if (isPositionValid(to)) {
      this.position = to;
    }
    return this.position;
  };
};

var RobotScript = function(script) {
  var self = this;

  this.script = (script || '').split(/\r?\n/);
  this.robot = new Robot();

  // Regular expressions to parse input data
  var lineParsers = [
    {
      regex: /^[LRM]+$/,
      callback: function(line) {
        return parseCommands(line);
      }
    },
    {
      regex: /^T \d+ \d+$/,
      callback: function(line) {
        return parseTeleport(line);
      }
    }
  ];

  function readHeader() {
    if (self.script.length < 2) {
      throw 'Header not found.';
    }

    var grid = self.parseGridLine(self.script[0]);
    var position = self.parsePositionLine(self.script[1]);
    self.robot = new Robot({
      grid: grid,
      position: position[0],
      direction: position[1]
    });
  }

  function parseCommands(line) {
    for (var i = 0; i < line.length; i++) {
      var cmd = line.charAt(i);
      if (cmd == 'L') {
        self.robot.turnLeft();
      } else if (cmd == 'R') {
        self.robot.turnRight();
      } else if (cmd == 'M') {
        self.robot.move();
      }
    }
  }

  function parseTeleport(line) {
    var tokens = line.split(/\s/);
    var x = parseInt(tokens[1]), y = parseInt(tokens[2]);
    self.robot.teleport([x,y]);
  }

  this.process = function() {
    readHeader();
    for (var i = 2; i < this.script.length; i++) {
      this.processCommands(this.script[i]);
    }

    var loc = this.robot.position;
    var dir = this.robot.direction;
    return loc[0] + ' ' + loc[1] + ' ' + dir;
  };

  this.parseGridLine = function(line) {
    if (!line.match(/^\d+ \d+$/)) {
      throw 'Grid dimension expected, ex: "10 10"';
    }
    var tokens = line.split(/\s/);
    return [parseInt(tokens[0]), parseInt(tokens[1])];
  };

  this.parsePositionLine = function(line) {
    if (!line.match(/^\d+ \d+ [NSEW]$/)) {
      throw 'Position data expected, ex: "5 2 N"';
    }
    var tokens = line.split(/\s/);
    return [[parseInt(tokens[0]), parseInt(tokens[1])], tokens[2]];
  };

  this.processCommands = function(line) {
    if (!line) {
      return;
    }

    var found = false;
    for (var i = 0; i < lineParsers.length; i++) {
      var parser = lineParsers[i];
      if (line.match(parser.regex)) {
        found = true;
        parser.callback(line);
        break;
      }
    }
    if (!found) {
      throw 'Invalid command line';
    }
  };
};
