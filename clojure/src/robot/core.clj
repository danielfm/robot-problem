(ns robot.core)

;; Direction vectors and transitions for all possible states
(def states {"N" [[0  1] ["W" "E"]]
	     "S" [[0 -1] ["E" "W"]]
	     "E" [[1  0] ["N" "S"]]
	     "W" [[-1 0] ["S" "N"]]})

(defn new-robot
  "Returns a new robot."
  [& {:keys [grid direction location]}]
  (let [g (or grid [10 10])
	d (or direction "N")
	l (or location [0 0])]
    (atom {:grid g :direction d :location l})))

(defn location-valid?
  "Returns whether the given [x y] coordinate is valid for the given robot."
  [robot-atom [x y]]
  (let [[gx gy] (:grid @robot-atom)]
    (and (>= x 0) (>= y 0) (< x gx) (< y gy))))

(defn turn-left!
  "Turns the given robot left while keeping its current direction."
  [robot-atom]
  (let [dir (last (states (:direction @robot-atom)))]
    (swap! robot-atom assoc :direction (first dir))))

(defn turn-right!
  "Turns the given robot right while keeping its current direction."
  [robot-atom]
  (let [dir (last (states (:direction @robot-atom)))]
    (swap! robot-atom assoc :direction (last dir))))

(defn teleport!
  "Moves the given robot to the [x y] coordinate."
  [robot-atom loc]
  (if (location-valid? robot-atom loc)
    (swap! robot-atom assoc :location loc)
    @robot-atom))

(defn move!
  "Moves the given robot one unit to the direction it's currently facing."
  [robot-atom]
  (let [[lx ly] (:location @robot-atom)
	[vx vy] (first (states (:direction @robot-atom)))
	loc [(+ lx vx) (+ ly vy)]]
    (teleport! robot-atom loc)))