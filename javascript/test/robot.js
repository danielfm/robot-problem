JsHamcrest.Integration.QUnit();

module('Robot', {
  setup: function() {
    this.robot = new Robot();
  }
});

test('should be initialized with sane defaults', function() {
  assertThat(this.robot.grid, [10,10]);
  assertThat(this.robot.position, [0,0]);
  assertThat(this.robot.direction, 'N');
});

test('should be initialized with a custom direction', function() {
  this.robot = new Robot({direction:'W'});
  assertThat(this.robot.grid, [10,10]);
  assertThat(this.robot.position, [0,0]);
  assertThat(this.robot.direction, 'W');
});

test('should be initialized with a custom grid size', function() {
  this.robot = new Robot({grid:[20,20]});
  assertThat(this.robot.grid, [20,20]);
  assertThat(this.robot.position, [0,0]);
  assertThat(this.robot.direction, 'N');
});

test('should be initialized with a custom position', function() {
  this.robot = new Robot({position:[5,2]});
  assertThat(this.robot.grid, [10,10]);
  assertThat(this.robot.position, [5,2]);
  assertThat(this.robot.direction, 'N');
});

test('should turn left', function() {
  assertThat(this.robot.turnLeft(), 'W');
  assertThat(this.robot.direction, 'W');
  assertThat(this.robot.turnLeft(), 'S');
  assertThat(this.robot.turnLeft(), 'E');
  assertThat(this.robot.turnLeft(), 'N');
  assertThat(this.robot.turnLeft(), 'W');
});

test('should turn right', function() {
  assertThat(this.robot.turnRight(), 'E');
  assertThat(this.robot.direction, 'E');
  assertThat(this.robot.turnRight(), 'S');
  assertThat(this.robot.turnRight(), 'W');
  assertThat(this.robot.turnRight(), 'N');
  assertThat(this.robot.turnRight(), 'E');
});

test('should teleport itself to another position', function() {
  assertThat(this.robot.teleport([2,3]), [2,3]);
  assertThat(this.robot.position, [2,3]);

  var grid = this.robot.grid;
  var limit = [grid[0]-1, grid[1]-1];
  assertThat(this.robot.teleport(limit), limit);
});

test('should not teleport itself outside the grid', function() {
  assertThat(this.robot.teleport([-1,-1]), [0,0]);
  assertThat(this.robot.teleport([-1,0]), [0,0]);
  assertThat(this.robot.teleport([0,-1]), [0,0]);
  assertThat(this.robot.teleport([0,10]), [0,0]);
  assertThat(this.robot.teleport([10,0]), [0,0]);
  assertThat(this.robot.teleport([10,10]), [0,0]);
  assertThat(this.robot.teleport([10,-1]), [0,0]);
});

test('should move forward', function() {
  assertThat(this.robot.move(), [0,1]);
  assertThat(this.robot.position, [0,1]);
  assertThat(this.robot.move(), [0,2]);

  this.robot.turnRight();
  assertThat(this.robot.move(), [1,2]);
  assertThat(this.robot.move(), [2,2]);
});

test('should not leave the grid', function() {
  this.robot.turnLeft();
  assertThat(this.robot.move(), [0,0]);
  this.robot.turnLeft();
  assertThat(this.robot.move(), [0,0]);

  // Upper boundaries
  this.robot.turnLeft();
  this.robot.teleport([9,9]);
  assertThat(this.robot.move(), [9,9]);
  this.robot.turnLeft();
  assertThat(this.robot.move(), [9,9]);
});

module('RobotScript', {
  setup: function() {
    this.script = new RobotScript();
  }
});

test('should parse the grid configuration line', function() {
  assertThat(this.script.parseGridLine('8 3'), [8,3]);
  assertThat(this.script.parseGridLine('10 12'), [10,12]);
});

test('should not parse an invalid grid configuration line', function() {
  assertThat(callTo(this.script.parseGridLine, '10 3A'), raisesAnything());
  assertThat(callTo(this.script.parseGridLine, 'A3 1'), raisesAnything());
});

test('should parse the position configuration line', function() {
  assertThat(this.script.parsePositionLine('8 3 N'), [[8,3], 'N']);
  assertThat(this.script.parsePositionLine('10 12 S'), [[10,12], 'S']);
});

test('should not parse an invalid position configuration line', function() {
  assertThat(callTo(this.script.parsePositionLine, '8 3'), raisesAnything());
  assertThat(callTo(this.script.parsePositionLine, '8 3 Z'), raisesAnything());
  assertThat(callTo(this.script.parsePositionLine, '10 3A S'), raisesAnything());
});

test('should process sequences of movement commands', function() {
  this.script.processCommands('LLRMRMR');
  var robot = this.script.robot;
  assertThat(robot.direction, 'E');
  assertThat(robot.position, [0,1]);
});

test('should ignore empty sequences of commands', function() {
  this.script.processCommands('');
  var robot = this.script.robot;
  assertThat(robot.direction, 'N');
  assertThat(robot.position, [0,0]);
});

test('should not process an invalid sequence of movement commands', function() {
  assertThat(callTo(this.script.processCommands, 'LLRXRMR'), raisesAnything());
});

test('should process a teleport command', function() {
  this.script.processCommands('T 6 2');
  var robot = this.script.robot;
  assertThat(robot.direction, 'N');
  assertThat(robot.position, [6,2]);
});

test('should not process an invalid teleport command', function() {
  assertThat(callTo(this.script.processCommands, 'T 62'), raisesAnything());
  assertThat(callTo(this.script.processCommands, 'T 6A 2'), raisesAnything());
  assertThat(callTo(this.script.processCommands, 'Z 6 2'), raisesAnything());
});

test('should process script', function() {
  this.script = new RobotScript('10 10\n2 5 N\nT 1 3\nLLRRMMMRLRMMM');
  assertThat(this.script.process(), '4 6 E');
});

test('should not process script without header', function() {
  assertThat(this.script.process, raisesAnything());
});

test('should process empty script with valid header', function() {
  this.script = new RobotScript('10 10\n3 5 N');
  assertThat(this.script.process(), '3 5 N');
});
