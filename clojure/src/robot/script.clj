(ns robot.script
  (:import [java.io IOException])
  (:use [robot.core])
  (:require [clojure.string :as string]))

(declare parse-commands! parse-teleport!)

;; Parsers for each type of input line
(def line-parsers [{:regex #"^[LRM]+$" :fn #'parse-commands!}
		   {:regex #"^T (\d+) (\d+)$" :fn #'parse-teleport!}])

(defn- to-int
  "Converts str to int."
  [str]
  (Integer/parseInt str))

(defn parse-grid-line
  "Parses the grid dimensions in line."
  [line]
  (let [matches (re-matches #"^(\d+) (\d+)$" line)]
    (if matches
      (map to-int (rest matches))
      (throw (IOException. "Grid dimensions expected, e.g. '10 10'")))))

(defn parse-location-line
  "Parses the location data in line."
  [line]
  (let [matches (re-matches #"^(\d+) (\d+) ([NSEW])$" line)
	pos (map to-int (take 2 (rest matches)))
	dir (last matches)]
    (if matches
      [pos dir]
      (throw (IOException. "Location data expected, e.g. '5 2 N'")))))

(defn create-robot
  "Creates a robot and sets its initial state based on the first two lines."
  [lines]
  (if (< (count lines) 2)
    (throw (IOException. "Header not found"))
    (let [[grid-line loc-line] (take 2 lines)
	  grid (parse-grid-line grid-line)
	  [loc dir] (parse-location-line loc-line)]
      (new-robot :grid grid :location loc :direction dir))))

(defn process-commands!
  "Processes an input line for the given robot."
  [line robot-atom]
  (doseq [parser line-parsers]
    (if-let [match (re-matches (:regex parser) line)]
      ((:fn parser) match robot-atom))))

(defn parse-commands!
  "Processes movement commands for the given robot."
  [commands robot-atom]
  (doseq [cmd commands]
    (case cmd
	  \L (turn-left! robot-atom)
	  \R (turn-right! robot-atom)
	  \M (move! robot-atom))))

(defn parse-teleport!
  "Processes a teleport command for the given robot."
  [match robot-atom]
  (let [loc (map to-int (rest match))]
    (teleport! robot-atom loc)))

(defn process
  "Processes each input line from script and returns the robot's final location
as a string, e.g. 'x y C', where x y is the coordinate and C is the facing
direction."
  [script]
  (let [lines (string/split-lines script)
	header (take 2 lines)
	body (drop 2 lines)
	robot (create-robot header)]
    (doseq [line body]
      (process-commands! line robot))
    (let [{[x y] :location dir :direction} @robot]
      (format "%d %d %s" x y dir))))